package com.me;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Manages acces to the JavaFx {@link Scene}.
 *
 * @author Matthias Braun
 *
 */
public final class StageManager {

    private static Stage stage;

    private StageManager() {

    }

    /**
     * Gets a {@link Node} by its ID in the scene.
     *
     * @param id
     *            the node's id without leading #
     * @return the node belonging to the {@code id}
     */
    public static Node getById(final String id) {
        return stage.getScene().lookup("#" + id);
    }

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(final Stage s) {
        stage = s;
    }

}
