package com.me.stepControllers;

import com.me.step.Step;
import com.me.step.StepListener;

public interface StepController extends StepListener {

    public void setStep(Step step);

}
