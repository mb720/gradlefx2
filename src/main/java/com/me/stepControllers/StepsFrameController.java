package com.me.stepControllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.me.StageManager;
import com.me.step.Step;

public class StepsFrameController {

    private static final String BTN_SUFFIX = "Btn";
    private static final String STEP_BTN_PREFIX = "step";
    /**
     * FXML files end with this string.
     */
    private static final String FXML_SUFFIX = ".fxml";
    /**
     * All FXML files describing the steps live here.
     */
    public static final String STEPS_DIR = "/gui/fxml/steps/";
    private static final int NR_OF_STEPS = 4;
    /**
     * All the FXML file names are called Step1.fxml, Step2.xml, etc.
     */
    private static final String STEP = "Step";
    private static Logger LOG = LoggerFactory
            .getLogger(StepsFrameController.class);

    /**
     * The {@link Node} wherein the content of one of the {@link Steps} is
     * displayed.
     * <p>
     * Only one step is shown at a time. The user can switch between steps by
     * clicking one of the buttons in the step frame.
     */
    @FXML
    HBox stepContent;
    @FXML
    Button prevStepBtn, nextStepBtn;

    /**
     * Associate every button Id with its corresponding step and vice versa
     */
    private final BiMap<String, Step> buttonsAndSteps = getButtonToStepsMap();
    private Step activeStep;

    public void showFirstStep() {
        LOG.info("Show first step");
        // Show the first step initially
        activeStep = buttonsAndSteps.get(STEP_BTN_PREFIX + 1 + BTN_SUFFIX);
        showStep();

    }

    @FXML
    protected void onNextStepBtnClick(final ActionEvent event) {

        LOG.info("Button was clicked: {}", event.getSource());
        if (activeStep.next() != null) {
            activeStep = activeStep.next();
            showStep();
        }
    }

    @FXML
    protected void onPrevStepBtnClick(final ActionEvent event) {

        LOG.info("Button was clicked: {}", event.getSource());
        if (activeStep.prev() != null) {
            activeStep = activeStep.prev();
            showStep();
        }

    }

    @FXML
    protected void onStepBtnClick(final ActionEvent event) {

        final ToggleButton clickedButton = (ToggleButton) event.getSource();
        // LOG.info("Button was clicked: {}", clickedButton);
        final Step stepToShow = buttonsAndSteps.get(clickedButton.getId());
        if (stepToShow == null) {
            LOG.warn("No step FXML is associated with button {}",
                    clickedButton.getId());
        } else {
            activeStep = stepToShow;
            showStep();
        }
    }

    /**
     * Creates a bidirectional mapping between button IDs and {@link Steps}s.
     * <p>
     * Every button ID must be correspond to a {@code FXML} file that describes
     * the GUI of a {@link Step}.
     *
     * @return a bidirectional mapping between buttons and steps
     */
    private BiMap<String, Step> getButtonToStepsMap() {
        final BiMap<String, Step> steps = HashBiMap.create(NR_OF_STEPS);
        Step prev = null;
        // The steps start at one
        for (int i = 1; i <= NR_OF_STEPS; i++) {
            // The first step is called "Step1.fxml"
            final String fxmlFile = STEPS_DIR + STEP + i + FXML_SUFFIX;
            final String buttonId = STEP_BTN_PREFIX + i + BTN_SUFFIX;
            final Step step = Step.create(fxmlFile, i, prev);
            // Add a controller to the step
            final StepController stepController = StepControllerFactory
                    .getStepController(i);
            stepController.setStep(step);
            // Let the controller receive events from the step
            step.addListener(stepController);

            steps.put(buttonId, step);
            if (prev != null) {
                prev.setNext(step);
            }
            prev = step;
        }
        return steps;
    }

    /**
     * Shows the active step in the GUI.
     *
     */
    private void showStep() {
        if (stepContent == null) {
            LOG.warn("There is no GUI component to show the step's content in: Check if there is a correct fx:id in the FXML.");
        } else {
            // Remove the content old step from the pane
            stepContent.getChildren().clear();
            try {
                final Node stepNode = activeStep.getNode();
                stepContent.getChildren().add(stepNode);
                // Expand the node inside its parent HBox
                HBox.setHgrow(stepNode, Priority.ALWAYS);
            } catch (final IOException e) {
                LOG.warn("Could not find GUI at {}",
                        activeStep.getPathToFxml(), e);
            }
        }
        // If the last step is shown, disable the "Next Step" button
        nextStepBtn.setDisable(activeStep.isLast());
        // If the fist step is shown, disable the "Previous Step" button
        prevStepBtn.setDisable(activeStep.isFirst());

        // Make sure the button belonging to the step is marked active
        final String buttonId = buttonsAndSteps.inverse().get(activeStep);
        final ToggleButton button = (ToggleButton) StageManager
                .getById(buttonId);
        button.setSelected(true);
    }

}
