package com.me.stepControllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StepControllerFactory {
    private static final Logger LOG = LoggerFactory
            .getLogger(StepControllerFactory.class);

    public static StepController getStepController(final int stepNumber) {
        final StepController controller;
        switch (stepNumber) {
        case 1:
            controller = new Step1Controller();
            break;
        case 2:
            controller = new Step2Controller();
            break;
        case 3:
            controller = new Step3Controller();
            break;
        case 4:
            controller = new Step4Controller();
            break;
        default:
            controller = null;
            LOG.warn("Unknown step number: {}", stepNumber);
            break;
        }
        return controller;
    }

}
