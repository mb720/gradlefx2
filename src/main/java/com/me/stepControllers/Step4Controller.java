package com.me.stepControllers;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.web.WebView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.me.step.Step;
import com.me.step.StepEvent;

public class Step4Controller extends AbstractStepController {
    private static final String HTTP_PREFIX = "http://";

    @FXML
    TextField urlField;
    private static final String DEFAULT_URL = "https://en.wikipedia.org/wiki/Main_Page";
    private static final Logger LOG = LoggerFactory
            .getLogger(Step4Controller.class);

    private Step controlledStep;

    private boolean browserAdded = false;

    /**
     * For unknown reasons, the webView becomes null after its first
     * initialization
     */
    private static WebView webView;

    @Override
    public void newStepEvent(final Step step, final StepEvent event) {
        if (event.equals(StepEvent.GUI_LOADED)) {
            addBrowserToGui();
        }

    }

    @Override
    public void setStep(final Step step) {
        this.controlledStep = step;
    }

    @FXML
    void goButtonClicked() {
        if (urlField == null) {
            LOG.warn("Why is the URL field null?");
        } else {
            final String userInput = urlField.getText();
            LOG.info("user input: {}", userInput);
            // Prepend the right protocol to the URL if necessary
            final String newUrl = userInput.startsWith(HTTP_PREFIX) ? userInput
                    : HTTP_PREFIX + userInput;
            webView.getEngine().load(newUrl);
        }
    }

    /**
     * Initially the step does not contain a browser. Add it after the step's
     * GUI has loaded.
     */
    private void addBrowserToGui() {
        if (!browserAdded) {
            final String selector = "#browserBox";
            HBox browserBox;
            try {
                browserBox = (HBox) controlledStep.getNode().lookup(selector);
                webView = new WebView();
                LOG.info("web view init");

                webView.getEngine().load(DEFAULT_URL);

                browserBox.getChildren().add(webView);
                // Expand the web view inside its parent HBox
                HBox.setHgrow(webView, Priority.ALWAYS);
                browserAdded = true;
            } catch (final IOException e) {
                LOG.warn("Could not find {} in node of step 4", selector, e);
            }
        }

    }

}
