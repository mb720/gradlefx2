package com.me;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.me.stepControllers.StepsFrameController;

public class Main extends Application {
    /**
     * The location of the frame GUI containing the individual steps
     */
    private static final String STEPS_FRAME_FXML = "/gui/fxml/StepsFrame.fxml";
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        // Load and show the GUI containing the step selection
        try {
            final FXMLLoader loader = new FXMLLoader(getClass().getResource(
                    STEPS_FRAME_FXML));
            final Scene scene = new Scene((Parent) loader.load());
            primaryStage.setScene(scene);
            primaryStage.show();
            // The manager provides access to the stage
            StageManager.setStage(primaryStage);

            // Show the first step initially
            final StepsFrameController frameController = loader.getController();
            frameController.showFirstStep();
        } catch (final Exception e) {
            LOG.error("Could not start GUI", e);
        }

    }

}
