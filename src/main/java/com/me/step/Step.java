package com.me.step;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

/**
 * A GUI element that guides the user to perform a generic step in a workflow.
 * <p>
 * The step is described using a FXML file.
 *
 * @author Matthias Braun
 *
 */
public class Step {

    /**
     * The path where this step's FXML is.
     */
    private final String pathToFxml;
    /**
     * The {@link Node} of this step.
     */
    private Node node = null;
    private final int stepNr;
    private Step nextStep;
    private final Step prevStep;
    private final Set<StepListener> stepListeners = new HashSet<>();

    private Step(final String pathToFxml, final int stepNr, final Step prev) {
        this.pathToFxml = pathToFxml;
        this.stepNr = stepNr;
        this.prevStep = prev;
    }

    public static Step create(final String fxmlFile, final int stepNr,
            final Step prev) {
        return new Step(fxmlFile, stepNr, prev);
    }

    public void addListener(final StepListener listener) {
        stepListeners.add(listener);

    }

    /**
     * @return This step as a {@link Node}. Loaded lazily.
     * @throws IOException
     *             thrown when the loading of the FXML that describes the step
     *             failed. Presumably because {@link #getPathToFxml()} returned
     *             the wrong path
     */
    public Node getNode() throws IOException {
        if (node == null) {
            final URL guiUrl = getClass().getResource(getPathToFxml());
            node = FXMLLoader.load(guiUrl);
            notifyStepListeners(StepEvent.GUI_LOADED);
        }
        return node;
    }

    public int getNr() {
        return stepNr;
    }

    /**
     *
     * @return The path to the FXML file that describes this step in terms of
     *         the GUI.
     */
    public String getPathToFxml() {
        return pathToFxml;
    }

    public boolean isFirst() {
        return prevStep == null;
    }

    public boolean isLast() {
        return nextStep == null;
    }

    public Step next() {
        return nextStep;
    }

    public Step prev() {
        return prevStep;
    }

    public void setNext(final Step next) {
        this.nextStep = next;

    }

    void notifyStepListeners(final StepEvent event) {
        for (final StepListener listener : stepListeners) {
            listener.newStepEvent(this, event);
        }

    }

}
