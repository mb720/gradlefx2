package com.me.step;

/**
 * A step event is something that happens to a {@link Step}.
 *
 * @author Matthias Braun
 *
 */
public enum StepEvent {
    /**
     * The GUI of a step has finished loading.
     */
    GUI_LOADED

}
