package com.me.step;

/**
 * Listeners get informed about different {@link StepEvent}s related to
 * {@link Step}s.
 *
 * @author Matthias Braun
 *
 */
public interface StepListener {
    public void newStepEvent(Step step, StepEvent event);

}
